package com.yry.ams.util;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;

/**
 * Created by Administrator on 2021/10/26.
 */
public class ImgCheck {
    private  MimetypesFileTypeMap mtftp;

    public final static ImgCheck imgCheck = new ImgCheck();


    public static ImgCheck getInstance(){
        return imgCheck;
    }

    private ImgCheck(){
        mtftp = new MimetypesFileTypeMap();
        /* 不添加下面的类型会造成误判 详见：http://stackoverflow.com/questions/4855627/java-mimetypesfiletypemap-always-returning-application-octet-stream-on-android-e*/
        mtftp.addMimeTypes("image png tif jpg jpeg bmp");
    }
    public  boolean isImage(File file){
        String mimetype= mtftp.getContentType(file);
        String type = mimetype.split("/")[0];
        return type.equals("image");
    }
}
