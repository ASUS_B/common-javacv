package com.yry.ams.config;

import com.yry.ams.util.BaiDuUtil;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Administrator on 2021/11/4.
 */
@Configuration
@ConditionalOnProperty(prefix = "baidu",name = {"clientId","clientSecret"},matchIfMissing = false)
@Data
public class BaiduConfig {

    /**
     * 应用的API Key
     */
    @Value("${baidu.clientId}")
    private String clientId;
    /**
     * 应用的Secret Key
     */
    @Value("${baidu.clientSecret}")
    private String clientSecret;

    @Bean
    public BaiDuUtil initBaiDuUtil(){
        return new BaiDuUtil(clientId,clientSecret);
    }
}
