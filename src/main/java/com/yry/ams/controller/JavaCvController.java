package com.yry.ams.controller;

import com.yry.ams.param.GrabVideoImgParam;
import com.yry.ams.param.ImgOCRParam;
import com.yry.ams.param.ImgToVideoParam;
import com.yry.ams.util.BaiDuUtil;
import com.yry.ams.util.JavacvUtils;
import net.sourceforge.tess4j.TesseractException;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.FrameRecorder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("javacvTest")
public class JavaCvController {
    @Autowired
    BaiDuUtil baiDuUtil;
    /**
     * 将图片和音频 合成一段视频
     * {"videoSavePath":"F:\\test2.mp4","imgPath":"C:\\Users\\Administrator\\Pictures\\Camera Roll","musicPath":"C:\\Users\\Administrator\\Music\\缦珠莎桦 - 不祥之恋.mp3"}
     *
     {"videoSavePath":"F:\\test3.mp4","imgList":["https://pic.netbian.com/uploads/allimg/211023/234248-16350037685cae.jpg","https://pic.netbian.com/uploads/allimg/211023/234117-16350036778d35.jpg"
     ,"https://pic.netbian.com/uploads/allimg/210630/194918-162505375895ca.jpg","https://pic.netbian.com/uploads/allimg/210906/233556-16309425562948.jpg","https://pic.netbian.com/uploads/allimg/210906/234737-1630943257c5c6.jpg"
     ,"https://pic.netbian.com/uploads/allimg/210611/215452-1623419692d5bc.jpg" ,"https://pic.netbian.com/uploads/allimg/210525/221208-1621951928937a.jpg" ,"https://pic.netbian.com/uploads/allimg/210517/222034-16212612346da0.jpg"
     ],"musicPath":"https://aod.cos.tx.xmcdn.com/storages/2201-audiofreehighqps/5B/3B/CKwRIMAFTsmbABUanADw29vO.m4a"}
     *
     * @param param
     * @return
     * @throws FrameRecorder.Exception
     * @throws FrameGrabber.Exception
     */
    @PostMapping("genVideoByImg")
    public Object genVideoByImg(@RequestBody ImgToVideoParam param) throws FrameRecorder.Exception, FrameGrabber.Exception {
        String fileNm =JavacvUtils.convertImgToMp4(param);
        return fileNm;
    }


    /**
     *  抓取视频图片
     *
     *  {"filePath":"http://upload.supersc.com/1595579648000-叮叮理财.mp4" ,"imgLocalSavePath":"F:\\grabVideo\\"}
     *  {"filePath":"F:\\test3.mp4","imgLocalSavePath":"F:\\grabVideo\\"}
     *
     * @param param
     * @return
     * @throws FrameRecorder.Exception
     * @throws FrameGrabber.Exception
     */
    @PostMapping("grabVideoImg")
    public Object grabVideoImg(@RequestBody GrabVideoImgParam param) throws FFmpegFrameGrabber.Exception {
        List<String> fileNmList =JavacvUtils.grabVideoImgSaveLocal(param);
        return fileNmList;
    }


    /**
     *  测试 图片文字识别
     * {"imagePath":"https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fblog%2F201408%2F28%2F20140828211009_jAKXF.thumb.700_0.jpeg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1638081523&t=b5b06343b6630a0f0ced2c498e96b827","dataPath":"F:\\tessdata","encode":"utf-8","lng":"chi_sim"}
     *
     * @param param
     * @return
     */
    @PostMapping("imgOCR")
    public Object imgOCR(@RequestBody ImgOCRParam param) throws TesseractException {
//        String imgText =JavacvUtils.imgOCR(param);
        String imgText =JavacvUtils.imgOCR_2(param);
        return imgText;
    }


    /**
     *  通用图片识别
     * {"imagePath":"https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fn.sinaimg.cn%2Fsinacn20120%2F529%2Fw929h1200%2F20190627%2F6cbc-hyzpvir5966568.jpg&refer=http%3A%2F%2Fn.sinaimg.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1638607414&t=d4118e803076d573523b09e273b7f453"}
     * @param param
     * @return
     */
    @PostMapping("imgDiscern_baidu")
    public Object imgDiscern_baidu(@RequestBody ImgOCRParam param) throws IOException {
        return baiDuUtil.imgGeneralDiscern(param.getImagePath());
    }
}
