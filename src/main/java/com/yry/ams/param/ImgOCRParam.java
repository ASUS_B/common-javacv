package com.yry.ams.param;

import lombok.Data;

/**
 * Created by Administrator on 2021/10/29.
 */
@Data
public class ImgOCRParam {
    /**
     * 语言类型 1.eng 英语 2.chi_sim 中文简体
     */
    String lng = "chi_sim";
    /**
     * 语言数据集文件夹路径
     */
    String dataPath;
    /**
     * 需要识别的图片文件路径
     */
    String imagePath;
    /**
     * 文字编码格式
     */
    String encode;

    /**
     * 图片做灰度处理存储位置
     */
    String grayImagePath;

    /**
     * 图片做灰度处理存储位置
     */
    boolean delgrayImage = true;
}
