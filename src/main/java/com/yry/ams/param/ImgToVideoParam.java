package com.yry.ams.param;

import lombok.Data;
import org.bytedeco.ffmpeg.global.avcodec;
import org.bytedeco.ffmpeg.global.avutil;

import java.util.List;

/**
 * Created by Administrator on 2021/10/26.
 */
@Data
public class ImgToVideoParam {
    /**
     * 视频本地存储地址
     */
    String videoSavePath;
    /**
     * 合成视频的图片地址
     */
    String imgPath;

    /**
     * 合成视频的图片地址
     */
    List<String> imgList;

    /**
     * 视频宽度
     */
    int imgWidth =1600;
    /**
     * 视频高度
     */
    int imgHeight =900;
    /**
     * 音频地址
     */
    String musicPath;
    /**
     * 视频的 每秒帧数
     */
    Double FrameRate = 1.0D;

    /**
     * 视频编码层模式 ,默认 mp4
     */
    int videoCodec = avcodec.AV_CODEC_ID_MPEG4;
    /**
     * 视频图像数据格式    默认 avutil.AV_PIX_FMT_YUV420P
     */
    int pixelFormat = avutil.AV_PIX_FMT_YUV420P;
    /**
     * 视频格式 , 后缀
     */
    String format ="mp4";
    /**
     * 运动因子   默认为1
     * 与视频中画面活动频繁程度有关，如果很频繁就设为4，不频繁则设为1
     */
    int motionFactory = 1;

    /**
     * 视频文件名称
     */
    String videoFileNm;
    /**
     * 是否根据帧数 重复图片
     *  true   每张图片 一秒, 根据 FrameRate 每秒帧数 重复图片
     *  false  每张图片 一帧
     */
    boolean rateRepeatImg= true;



}
