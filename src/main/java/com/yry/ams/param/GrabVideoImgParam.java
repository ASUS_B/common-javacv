package com.yry.ams.param;

import lombok.Data;

/**
 * Created by Administrator on 2021/10/28.
 */
@Data
public class GrabVideoImgParam {
    /**
     *   视频文件路劲
     */
    String filePath;
    /**
     * 起始抓取 帧位置
     */
    Integer start;
    /**
     * 结束抓取 帧位置
     */
    Integer end;
    /**
     * 解析后的图片本地保存地址
     */
    String imgLocalSavePath;
    /**
     * 图片保存后缀名，默认jpg
     */
    String imageMat ="jpg";
}
