package com.yry.ams.ex;

/**
 * 自定义异常
 */
public class BaseErrorException extends RuntimeException{

    private static final long serialVersionUID = -2470461654663264392L;

    private Integer errorCode;
    private String message;

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BaseErrorException() {
        super();
    }

    public BaseErrorException(String message) {
        super(message);
        this.message = message;
    }

    public BaseErrorException(Integer errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
        this.message = message;
    }

    public BaseErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseErrorException(Throwable cause) {
        super(cause);
    }

}
