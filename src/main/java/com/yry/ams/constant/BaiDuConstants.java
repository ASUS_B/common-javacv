package com.yry.ams.constant;

/**
 * 百度AIP开放平台
 */
public class BaiDuConstants {
    /**
     *   通用鉴权 获取Access Token
     */
    public static final String REQ_ACCESS_TOKEN_URL="https://aip.baidubce.com/oauth/2.0/token";

    /**
     * 通用物体和场景识别
     */
    public static final String GENERAL_DISCERN_URL="https://aip.baidubce.com/rest/2.0/image-classify/v2/advanced_general";


}
