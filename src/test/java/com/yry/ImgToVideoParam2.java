package com.yry;

import lombok.Data;
import org.bytedeco.ffmpeg.global.avcodec;
import org.bytedeco.ffmpeg.global.avutil;

import java.util.List;

/**
 * Created by Administrator on 2021/10/26.
 */
@Data
public class ImgToVideoParam2 {
    /**
     * 视频本地存储地址
     */
    String videoSavePath;

    /**
     * 合成视频的图片地址
     */
    List<String> imgList;

    /**
     * 视频宽度
     */
    int imgWidth =1600;
    /**
     * 视频高度
     */
    int imgHeight =900;
    /**
     * 音频地址
     */
    String musicPath;



}
