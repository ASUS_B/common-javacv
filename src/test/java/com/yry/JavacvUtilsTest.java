package com.yry;

import com.yry.ams.ex.BaseErrorException;
import com.yry.ams.util.ImgCheck;
import org.bytedeco.ffmpeg.global.avcodec;
import org.bytedeco.ffmpeg.global.avutil;
import org.bytedeco.javacv.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2021/10/28.
 */
public class JavacvUtilsTest {

    /**
     * 设置 视频的 每秒帧数； 25帧每秒 ，即表示  一秒 25 张图片
     */
    private static final double FRAME_RATE = 25.0;
    /**
     * 运动因子
     */
    private static final double MOTION_FACTOR = 1;


    /**
     * 图片合成 mp4 类型的视频
     * 建议合成的图片宽高要一致，并且视频的宽高还是要符合一定比例
     * @param mp4SavePath  视频地址
     * @param imgPath   图片地址
     * @param width     视频宽度
     * @param height     视频高度
     * @throws FrameRecorder.Exception
     */
    public static void convertImgToMp4(String mp4SavePath,String imgPath, int width, int height) throws FrameRecorder.Exception {
        //读取所有图片
        File file = new File(imgPath);
        File[] files = file.listFiles();
        if(files==null||files.length<1){
            throw new BaseErrorException("对应路劲【"+imgPath+"】下无文件");
        }
        Map<Integer, File> imgMap = new HashMap<Integer, File>();
        int num = 0;
        for (File imgFile : files) {
            if(ImgCheck.getInstance().isImage(imgFile)){
                imgMap.put(num, imgFile);
                num++;
            }
        }
        // 处理视频宽高
        formatWidthAndHeight(width,height);

        //视频宽高最好是按照常见的视频的宽高  16：9  或者 9：16
        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(mp4SavePath, width, height);
        //设置视频编码层模式
        recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
        //设置视频为25帧每秒
        recorder.setFrameRate(FRAME_RATE);
        /*
        * videoBitRate这个参数很重要，这个说明视频每秒大小，值越大，值越大图片压缩率越小，视频质量越高，但最终的生成的视频也越大。
        * 均衡考虑建议设为videoWidth*videoHeight*frameRate*0.07*运动因子，运动因子则与视频中画面活动频繁程度有关，如果很频繁就设为4，不频繁则设为1
        */
        recorder.setVideoBitrate((int)((width*height*FRAME_RATE)*MOTION_FACTOR*0.07));

        //设置视频图像数据格式
        recorder.setPixelFormat(avutil.AV_PIX_FMT_YUV420P);
        recorder.setFormat("mp4");

//        recorder.setTimestamp();
        try {
            recorder.start();
            Java2DFrameConverter converter = new Java2DFrameConverter();
            //录制一个22秒的视频
            for (int i = 0,length=imgMap.size(); i <length ; i++) {
                BufferedImage read = ImageIO.read(imgMap.get(i));
                //一秒是25帧 所以要记录25次
                for (int j = 0; j < FRAME_RATE; j++) {
                    recorder.record(converter.getFrame(read));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //最后一定要结束并释放资源
            recorder.stop();
            recorder.release();
        }
    }

    /**
     * 图片合成 mp4 类型的视频
     * 建议合成的图片宽高要一致，并且视频的宽高还是要符合一定比例
     * @param imgToVideo
     * @throws FrameRecorder.Exception
     */
    public static void convertImgToMp4(ImgToVideoParam2 imgToVideo) throws FrameRecorder.Exception {
        // 处理视频宽高
        formatWidthAndHeight(imgToVideo.getImgWidth(),imgToVideo.getImgHeight());

        //视频宽高最好是按照常见的视频的宽高  16：9  或者 9：16
        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(imgToVideo.getVideoSavePath(), imgToVideo.getImgWidth(),imgToVideo.getImgHeight());
        //设置视频编码层模式
        recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
        //设置视频为25帧每秒
        recorder.setFrameRate(FRAME_RATE);
        /*
        * videoBitRate这个参数很重要，当然越大，越清晰，但最终的生成的视频也越大。
        * 查看一个资料，说均衡考虑建议设为videoWidth*videoHeight*frameRate*0.07*运动因子，运动因子则与视频中画面活动频繁程度有关，如果很频繁就设为4，不频繁则设为1
        */
        recorder.setVideoBitrate((int)((imgToVideo.getImgWidth()*imgToVideo.getImgHeight()*FRAME_RATE)*MOTION_FACTOR*0.07));

        //设置视频图像数据格式
        recorder.setPixelFormat(avutil.AV_PIX_FMT_YUV420P);
        recorder.setFormat("mp4");

//        recorder.setTimestamp();
        try {
            recorder.start();
            Java2DFrameConverter converter = new Java2DFrameConverter();
            //录制一个22秒的视频
            List<String> imgUrlList = imgToVideo.getImgList();
            for (int i = 0,length=imgUrlList.size(); i <length ; i++) {
                URL imgUrl = new URL(imgUrlList.get(i));
                BufferedImage read = ImageIO.read(imgUrl);
                //一秒是25帧 所以要记录25次
                for (int j = 0; j < FRAME_RATE; j++) {
                    recorder.record(converter.getFrame(read));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //最后一定要结束并释放资源
            recorder.stop();
            recorder.release();
        }
    }

    /**
     *  视频的宽度必须是32的倍数，高度必须是2的倍数
     * @param width
     * @param height
     */
    private static void formatWidthAndHeight(int width,int height){
        if (width % 32 != 0) {
            int j = width % 32;
            if (j <= 16) {
                width = width - (width % 32);
            } else {
                width = width + (32 - width / 32);
            }
        }
        if (height % 2 != 0) {
            int j = height % 4;
            switch (j) {
                case 1:
                    height = height - 1;
                    break;
                case 3:
                    height = height + 1;
                    break;
            }
        }

    }

    /**
     * 图片合成 mp4 类型的视频
     * 建议合成的图片宽高要一致，并且视频的宽高还是要符合一定比例
     * @param mp4SavePath  视频地址
     * @param imgPath   图片地址
     * @param width     视频宽度
     * @param height     视频高度
     * @throws FrameRecorder.Exception
     */
    public static void convertImgAndMusicToMp4(String mp4SavePath,String imgPath, int width, int height,String musicPath) throws FrameRecorder.Exception, FrameGrabber.Exception {
        //读取所有图片
        File file = new File(imgPath);
        File[] files = file.listFiles();
        if(files==null||files.length<1){
            throw new BaseErrorException("对应路劲【"+imgPath+"】下无文件");
        }
        Map<Integer, File> imgMap = new HashMap<Integer, File>();
        int num = 0;
        for (File imgFile : files) {
            if(ImgCheck.getInstance().isImage(imgFile)){
                imgMap.put(num, imgFile);
                num++;
            }
        }
        // 处理视频宽高
        formatWidthAndHeight(width,height);

        // 帧 抓取器
//        FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(new File(musicPath) );
        FFmpegFrameGrabber grabber =FFmpegFrameGrabber.createDefault(new File(musicPath) );

        //视频宽高最好是按照常见的视频的宽高  16：9  或者 9：16
        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(mp4SavePath, width, height);

        //设置视频编码层模式
        recorder.setVideoCodec(avcodec.AV_CODEC_ID_MPEG4);
        //设置视频为25帧每秒
        recorder.setFrameRate(FRAME_RATE);
        /*
        * videoBitRate这个参数很重要，当然越大，越清晰，但最终的生成的视频也越大。
        * 查看一个资料，说均衡考虑建议设为videoWidth*videoHeight*frameRate*0.07*运动因子，运动因子则与视频中画面活动频繁程度有关，如果很频繁就设为4，不频繁则设为1
        */
        recorder.setVideoBitrate((int)((width*height*FRAME_RATE)*MOTION_FACTOR*0.07));

        //设置视频图像数据格式
        recorder.setPixelFormat(avutil.AV_PIX_FMT_YUV420P);
//        recorder.setFormat("mp4");

        // 不可变(固定)音频比特率
//        recorder.setAudioOption("crf", "0");
//        //最高质量
//        recorder.setAudioQuality(0);
//         音频采样率
//        recorder.setSampleRate(grabber.getSampleRate());
//        recorder.setSampleRate((int)FRAME_RATE);
//         双通道(立体声)
        recorder.setAudioChannels(2);
//         音频比特率
//        recorder.setAudioBitrate(grabber.getAudioBitrate());
        // 告诉录制器这个audioSamples的音频时长
//        recorder.setTimestamp(grabber.getTimestamp());

//        recorder.setAudioCodec(avcodec.AV_CODEC_ID_AAC);

        Integer audioChannels = recorder.getAudioChannels();
        try {
            recorder.start();
            System.out.println("recorder的AudioChannels== "+audioChannels);
            Java2DFrameConverter converter = new Java2DFrameConverter();
            //录制一个22秒的视频
            for (int i = 0,length=imgMap.size(); i <length ; i++) {
                BufferedImage read = ImageIO.read(imgMap.get(i));
                //录入每一帧的图片    ，因为 一秒是25帧 所以要记录25次
                for (int j = 0; j < FRAME_RATE; j++) {
                    //录入图片
                    recorder.record(converter.getFrame(read));
                }
            }
            grabber.start();// 开始录制音频
            Frame audioSamples;
            int x = 1;
            while ((audioSamples = grabber.grabFrame()) != null) {
                if(audioSamples.samples  != null){
                    System.out.println("进入录制音频-======");
//                    recorder.setTimestamp(audioSamples.timestamp);
                    recorder.record(audioSamples); // 录入音频
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            grabber.stop();
            grabber.release();
            recorder.stop();
            recorder.release();
        }
    }

}
